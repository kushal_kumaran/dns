calcium = "192.121.167.154";

a("potassium", "162.244.28.152");
a("chlorine", "162.244.27.68");
a("calcium", calcium);

a("mail", calcium);
mx(_a, "mail.locationd.net", 10);

potassium_fqdn = "potassium.locationd.net.";
chlorine_fqdn = "chlorine.locationd.net.";
calcium_fqdn = "calcium.locationd.net.";

cname("magnesium", calcium_fqdn);
cname("chat", chlorine_fqdn);
cname("cloud", calcium_fqdn);
cname("ethercalc", chlorine_fqdn);
cname("etherpad", chlorine_fqdn);
cname("ldap", chlorine_fqdn);
cname("media", calcium_fqdn);
cname("vcs", calcium_fqdn);

txt("mail._domainkey", "v=DKIM1; k=rsa; t=y; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9o2pZ4uA9Xx3oqAbQVZn+Nt6cXtGS0oJCaBkB25j0+HeweM4bmrvoiUKiU3//PuusGA3XfPbI9Leuir1SVfBX7FFcW931xzkX3xGTZK0uHZZJlEu3shBeRIsYDzBhTFDngnyqD+O45VKAF0ttWCM1La+VAQ0Sc7ppBbHBiwAZswIDAQAB");
